package com.axway.controller;


import com.axway.entinies.Actor;
import com.axway.entinies.Genre;
import com.axway.entinies.Movie;
import com.axway.repositories.ActorRepository;
import com.axway.repositories.MovieRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieController {
    private ActorRepository actorRepository;

    private MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @RequestMapping("/movies")
    public String getMovies(Model model,
                            @RequestParam(value = "title", required = false)
                                    String title
    ) {

        Iterable<Movie> allMoviesI = movieRepository.findAll();
        List<Movie> allMovies = (List<Movie>) allMoviesI;
        List<Movie> filteredMovies;


        if (title == null) {
            filteredMovies = (List<Movie>) allMovies;


        } else {

            filteredMovies = getFilterMovies(title, allMovies);


        }

        model.addAttribute("movies", filteredMovies);


        return "movies";


    }


    @RequestMapping(value = "/movies/{id}", method = RequestMethod.GET)
    public String getMoviesById(@PathVariable(value = "id") long id, Model model) {

        Movie movie = movieRepository.findById(id).get();
        model.addAttribute("movies", movie);
        return "movie-details";
    }


    private List<Movie> getFilterMovies(String title, List<Movie> allMovies) {

        List<Movie> filteredMovie = new ArrayList<>();

        for (Movie m : allMovies) {


            if (m.getTitle().toLowerCase().contains(title.toLowerCase())) {
                filteredMovie.add(m);
            }
        }

        return filteredMovie;
    }

    @RequestMapping(value = "/deleteMovie", method = RequestMethod.POST)
    private String deletMovie(@RequestParam long id) {
        movieRepository.deleteById(Long.valueOf(id));
        return "redirect:/movies";
    }


    @PostMapping("/movies")
    public String addNewMovie(
            Model model,
            @ModelAttribute
                    Movie newMovie
    ) throws FileNotFoundException {
        System.out.println(newMovie.getTitle());

        boolean exist = false;

        Iterable<Movie> allMoviesI = movieRepository.findAll();
        List<Movie> allMovies = (List<Movie>) allMoviesI;


        for (Movie m : allMovies) {
            if ((m.getTitle().equals(newMovie.getTitle()))) {
                exist = true;
            }

        }
        if (exist) {
            return "movie-exist";

        } else {
//            File file = ResourceUtils.getFile("classpath:imdb/title.tsv");
//            System.out.println("File Found : " + file.exists());
//            String info =readFromTitle(file, newMovie.getTitle(), newMovie.getDateOfRelease());
//
//
//            System.out.println(info);
//            if (!info.isEmpty()) {
//                String[] infoArr = info.split("\t");
//
//              //  Genre newGenre = new Genre();
//             //   newGenre.setName(infoArr[8]);
//              //  newMovie.addGenre(newGenre);
//
//                newMovie.setImdbID(infoArr[0]);
//                ActorController AC = new ActorController(actorRepository);
//                List<Actor> movieAcorsArr = AC.getActorsFromIMDB(infoArr[0], newMovie.getImdbID());
//                if (!movieAcorsArr.isEmpty()){
//                    for (Actor a: movieAcorsArr
//                         ) {
//                        newMovie.addActor(a);
//
//                    }
//
//                }
//            }

            movieRepository.save(newMovie);



        }
        return "movie-added";

    }

    @RequestMapping(value = "/movie-update/{id}", method = RequestMethod.GET)
    public String updateMovie(@PathVariable(value = "id") long id,
                              Model model) {

        Movie movie = movieRepository.findById(id).get();
        model.addAttribute("movies", movie);
        return "movie-update";
    }

    @PostMapping(value = "/movie-update1")
    public String updateMovie(@RequestParam long id,
                                 Model model,
                                 @ModelAttribute
                                         Movie newMovie
    ) {


        boolean exist = false;

        Iterable<Movie> allMoviesI = movieRepository.findAll();
        List<Movie> allMovies = (List<Movie>) allMoviesI;


        for (Movie m : allMovies) {
            if ((m.getTitle().equals(newMovie.getTitle()))) {
                exist = true;
            }

        }
        if (exist) {
            return "redirect:/movies";

        } else {
            newMovie.setId(id);
            movieRepository.save(newMovie);



        }
        return "redirect:/movies";

    }


    @RequestMapping(value = "/removeActor")
    public String removeActorFromMovie(@RequestParam(value = "id") long id,
                                       @RequestParam(value="actorId") long actorId,
                                       HttpServletRequest request
    ) {


//
//        Movie movie = movieRepository.findById(id).get();
//
//
//        movie.removeActor(actorId);
//        movieRepository.save(movie);



        return "redirect:/movies";

    }

    public static String readFromTitle(File titleFile, String titleName, int dateOfRelease) {
        String strCurrentLine = null;
        BufferedReader objReader = null;
        try {


            objReader = new BufferedReader(new FileReader(titleFile));

            while ((strCurrentLine = objReader.readLine()) != null) {

                String[] currentArr = strCurrentLine.split("\t");
                if ((currentArr[2].equals(titleName))&&((Integer.valueOf(currentArr[5])==dateOfRelease)))
                {

                    System.out.println(strCurrentLine);
                    break;
                }
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {
                if (objReader != null)
                    objReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return strCurrentLine;
    }


}
