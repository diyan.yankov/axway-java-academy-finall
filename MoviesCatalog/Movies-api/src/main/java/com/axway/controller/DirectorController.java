package com.axway.controller;

import com.axway.entinies.Director;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.axway.repositories.DirectorRepository;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class DirectorController  {
    Director newDirector;

    private List<Director> director;

    private DirectorRepository directorRepository;

    public DirectorController(DirectorRepository directorRepository){
        this.directorRepository = directorRepository;
    }

    @RequestMapping("/directors")
    public String getDirectors(Model model,
                            @RequestParam(value = "firstName", required = false)
                                    String firstName
    ) {

        Iterable<Director> allDirectorsI = directorRepository.findAll();
        List<Director>  allDirectors = (List<Director>) allDirectorsI;
        List<Director> filteredDirectors;




        if (firstName == null) {
            filteredDirectors = (List<Director>) allDirectors;


        } else {

            filteredDirectors = getFilterDirectors(firstName, allDirectors);



        }

        model.addAttribute("directors", filteredDirectors);


        return "directors";



    }


    @RequestMapping(value = "/directors/{id}", method = RequestMethod.GET)
    public String getDirectorById(@PathVariable(value = "id") long id, Model model) {

        Director director = directorRepository.findById(id).get();
        model.addAttribute("directors", director);
        return "directors-details";
    }


    @RequestMapping(value = "/director-update/{id}", method = RequestMethod.GET)
    public String updateDirector(@PathVariable(value = "id") long id, Model model) {

        Director director = directorRepository.findById(id).get();
        model.addAttribute("directors", director);
        return "director-update";
    }


    private List<Director> getFilterDirectors(String firstName, List<Director> allDirectors) {

        List<Director> filteredDirector = new ArrayList<>();

        for (Director a : allDirectors) {


            if (a.getFirstName().toLowerCase().contains(firstName.toLowerCase())|| a.getLastName().toLowerCase().contains(firstName.toLowerCase())) {
                filteredDirector.add(a);
            }
        }

        return filteredDirector;
    }


    @RequestMapping(value = "/deleteDirector", method = RequestMethod.POST)
    private String deleteDirector(@RequestParam long id){
        directorRepository.deleteById(Long.valueOf(id));
        return "redirect:/directors";
    }

    @PostMapping("/directors")
    public String addNewDirector(
            Model model,
            @ModelAttribute
                    Director newDirector
    ) {
        System.out.println(newDirector.getFirstName());

        boolean exist = false;

        Iterable<Director> allDirectorI = directorRepository.findAll();
        List<Director>  allDirectors = (List<Director>) allDirectorI;




        for (Director a : allDirectors){
            if(a.equals(newDirector)){
                exist = true;
            }
        }




        if (exist){
            return "director-exist";

        }else {
            directorRepository.save(newDirector);


            return "director-added";
        }
    }


    @PostMapping(value = "/updateDirector1")
    public String updateNewDirector(@RequestParam long id,
                                 Model model,
                                 @ModelAttribute
                                         Director newDirector
    ) {
        System.out.println(newDirector.getFirstName());
        System.out.println(directorRepository.findById(id));


        boolean exist = false;

        Iterable<Director> allDirectorsI = directorRepository.findAll();
        List<Director>  allDirectors = (List<Director>) allDirectorsI;

        for (Director a : allDirectors){
            if((a.toString().equals(newDirector.toString()))){
                exist = true;
            }


        }

        if (exist){
            return "director-exist";
        }

        newDirector.setId(id);
        directorRepository.save(newDirector);








        return "director-added";
    }










}






