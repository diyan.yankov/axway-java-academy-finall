package com.axway.controller;

import com.axway.entinies.Actor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import com.axway.repositories.ActorRepository;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ActorController  {
    Actor newActor;

    private List<Actor> actor;

    private ActorRepository actorRepository;

 //   public ActorController(){};


    public ActorController(ActorRepository actorRepository){
        this.actorRepository = actorRepository;
    }

    @RequestMapping("/actors")
    public String getActors(Model model,
                            @RequestParam(value = "firstName", required = false)
                                    String firstName
                            ) {

        Iterable<Actor> allActorsI = actorRepository.findAll();
        List<Actor>  allActors = (List<Actor>) allActorsI;
        List<Actor> filteredActors;




        if (firstName == null) {
            filteredActors = (List<Actor>) allActors;


        } else {

            filteredActors = getFilterActors(firstName, allActors);



        }

        model.addAttribute("actors", filteredActors);


        return "actors";



    }


        @RequestMapping(value = "/actors/{id}", method = RequestMethod.GET)
        public String getActorById(@PathVariable(value = "id") long id, Model model) {

        Actor actor = actorRepository.findById(id).get();
            model.addAttribute("actors", actor);
            return "actors-details";
        }


    @RequestMapping(value = "/actor-update/{id}", method = RequestMethod.GET)
    public String updateActor(@PathVariable(value = "id") long id, Model model) {

        Actor actor = actorRepository.findById(id).get();
        model.addAttribute("actors", actor);
        return "actor-update";
    }


    private List<Actor> getFilterActors(String firstName, List<Actor> allActors) {

        List<Actor> filteredActor = new ArrayList<>();

        for (Actor a : allActors) {


            if (a.getFirstName().toLowerCase().contains(firstName.toLowerCase())|| a.getLastName().toLowerCase().contains(firstName.toLowerCase())) {
                filteredActor.add(a);
            }
        }

        return filteredActor;
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    private String deleteActor(@RequestParam long id){
        actorRepository.deleteById(Long.valueOf(id));
        return "redirect:/actors";
    }

    @PostMapping("/actors")
    public String addNewActor(
            Model model,
            @ModelAttribute
                    Actor newActor
    ) {
        System.out.println(newActor.getFirstName());

        boolean exist = false;

        Iterable<Actor> allActorsI = actorRepository.findAll();
        List<Actor>  allActors = (List<Actor>) allActorsI;




        for (Actor a : allActors){
            if(a.equals(newActor)){
                exist = true;
            }
        }




        if (exist){
            return "actor-exist";

        }else {
            actorRepository.save(newActor);


            return "actor-added";
        }
    }


    @PostMapping(value = "/updateActor1")
    public String updateNewActor(@RequestParam long id,
            Model model,
            @ModelAttribute
                    Actor newActor
    ) {
        System.out.println(newActor.getFirstName());
        System.out.println(actorRepository.findById(id));


        boolean exist = false;

        Iterable<Actor> allActorsI = actorRepository.findAll();
        List<Actor>  allActors = (List<Actor>) allActorsI;

        for (Actor a : allActors){
            if((a.toString().equals(newActor.toString()))){
                exist = true;
            }


        }

        if (exist){
            return "actor-exist";
        }

        newActor.setId(id);
        actorRepository.save(newActor);








            return "actor-added";
        }





    @RequestMapping(value = "/movie-update/removeActor1{id}/{actorId}", method = RequestMethod.POST)
    public String removeActorFromMovie(@PathVariable(value = "id")long id,
                                       @PathVariable(value = "actorId") long actorId,
                                       HttpServletRequest request
    ) {


//
//        Movie movie = movieRepository.findById(id).get();
//
//
//        movie.removeActor(actorId);
//        movieRepository.save(movie);



        return "redirect:/movies";

    }


    public  List<Actor> getActorsFromIMDB(String id, String IdImdb) throws FileNotFoundException {
        File file = ResourceUtils.getFile("classpath:imdb/actors.tsv");
        System.out.println("File Found : " + file.exists());
        List<Actor> newActors = null;




        Iterable<Actor> allActorsI = actorRepository.findAll();
        List<Actor>  allActors = (List<Actor>) allActorsI;


        String strCurrentLine = null;
        BufferedReader objReader = null;
        try {


            objReader = new BufferedReader(new FileReader(file));

            while ((strCurrentLine = objReader.readLine()) != null) {

               String[] currentArr = strCurrentLine.split("\t");
               String[] isActor = currentArr[4].split(",");
                if ((strCurrentLine.contains(IdImdb)))
                {
                    if (isActor[0].equals("actor")||(isActor.equals("actress"))) {
                        boolean isInt = isStringInt(currentArr[2]);
                        if (isInt){
                            Actor myActor = newActor;
                            String[] name = currentArr[1].split(" ");
                            myActor.setFirstName(name[0]);
                            myActor.setLastName(name[1]);
                            myActor.setDateOfBirth(Integer.parseInt(currentArr[2]));

                            for (Actor a : allActors){
                                if(!(a.toString().equals(myActor.toString()))){
                                    actorRepository.save(myActor);

                                }else {
                                    myActor = a;
                                }




                            }
                            newActors.add(myActor);

                        }

                    }

                }
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {
                if (objReader != null)
                    objReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return newActors;
    }
    public static boolean isStringInt(String s)
    {
        try
        {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex)
        {
            return false;
        }
    }





}






