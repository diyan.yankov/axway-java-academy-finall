package com.axway.importData;

import com.axway.entinies.Actor;
import com.axway.entinies.Director;
import com.axway.entinies.Movie;
import com.axway.repositories.DirectorRepository;
import com.axway.repositories.MovieRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import com.axway.repositories.ActorRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class importData implements ApplicationListener<ContextRefreshedEvent> {

    private ActorRepository actorRepository;
    private DirectorRepository directorRepository;
    private MovieRepository movieRepository;



    public importData(ActorRepository actorRepository, DirectorRepository directorRepository, MovieRepository movieRepository){
        this.actorRepository = actorRepository;
        this.directorRepository = directorRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void  initData() {

        Director lucas = new Director();
        lucas.setFirstName("Lucas");
        lucas.setLastName("Jones");

        lucas.setDateOfBirth(1944);


        Director peter = new Director();
        peter.setFirstName("Peter");
        peter.setLastName("Jackson");

        peter.setDateOfBirth(1961);


        directorRepository.save(lucas);
        directorRepository.save(peter);

        Actor tom = new Actor();
        tom.setFirstName("Tom");
        tom.setLastName("Cruise");
        //LocalDate tomDate = LocalDate.of(1962, 6, 3);
        tom.setDateOfBirth(1962);


        Actor pit = new Actor();
        pit.setFirstName("Brad");
        pit.setLastName("Pitt");
       // LocalDate pittDate = LocalDate.of(1963, 12, 18);
        pit.setDateOfBirth(1963);


        Actor johnny = new Actor();
        johnny.setFirstName("Johnny");
        johnny.setLastName("Depp");
        //LocalDate johnnyDate = LocalDate.of(1963, 6, 9);
        johnny.setDateOfBirth(1963);

        actorRepository.save(tom);
        actorRepository.save(pit);
        actorRepository.save(johnny);



        Director stiven = new Director();
        stiven.setFirstName("Steven");
        stiven.setLastName("Spilberg");
        stiven.setDateOfBirth(1946);

        Director quentin = new Director();
        quentin.setFirstName("Quentin");
        quentin.setLastName("Tarantino");
        quentin.setDateOfBirth(1963);

        Director alfred = new Director();
        alfred.setFirstName("Alfred");
        alfred.setLastName("Hitchcock");
        alfred.setDateOfBirth(1899);

        Director david = new Director();
        david.setFirstName("David");
        david.setLastName("Fincher");
        david.setDateOfBirth(1962);



        directorRepository.save(stiven);
        directorRepository.save(quentin);
        directorRepository.save(alfred);
       directorRepository.save(david);

        Movie LOTR = new Movie();
        LOTR.setTitle("Lord of the Rings");
        LOTR.setDescription("A great movie");
        LOTR.setDateOfRelease(1962);


        Movie SW = new Movie();
        SW.setTitle("Star Wars");
        SW.setDescription("Jedi an Sith stuff");
        SW.setDateOfRelease(1988);


        Movie troy = new Movie();
        troy.setTitle("TROY");
        troy.setDescription("Brad Pit running half naked");
        troy.setDateOfRelease(2000);

        movieRepository.save(LOTR);
        movieRepository.save(SW);
        movieRepository.save(troy);




        Movie fight = new Movie();
        fight.setTitle("Fight Club");
        fight.addActor(pit);
        fight.setDateOfRelease(1999);
        fight.setDirector(david);
        fight.setDescription("very good movie");

        movieRepository.save(fight);

        pit.addMovie(fight);
        actorRepository.save(pit);
        System.out.println(pit.getMovies());











    }


}
