package com.axway.repositories;

import com.axway.entinies.Director;

import org.springframework.data.repository.CrudRepository;


public interface DirectorRepository extends CrudRepository<Director, Long> {

}
