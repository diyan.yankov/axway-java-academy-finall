package com.axway.repositories;


import com.axway.entinies.Movie;
import org.springframework.data.repository.CrudRepository;

public interface MovieRepository extends  CrudRepository <Movie, Long>{

}