package com.axway.repositories;

import com.axway.entinies.Genre;
import org.springframework.data.repository.CrudRepository;


public interface GenreRepository extends  CrudRepository<Genre, Long>{

        }
