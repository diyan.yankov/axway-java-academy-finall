Movie Catalog





Running locally
run class: RunApplication


Configuration
Environment Variable	Default	Description
http://localhost:8080/movies

database:

http://localhost:8080/h2-console
user: sa

Technologies
h2database
springframework
hibernate

for automatic creation of movies and actors: not working for now
download from https://datasets.imdbws.com/ 
name.basics.tsv.gz
title.basics.tsv.gz
title.crew.tsv.gz
make directory in resources imdb and extract the files with changed names as follows:
title.basics.tsv.gz > titles.tsv
name.basics.tsv.gz > actors.tsv
title.crew.tsv.gz > directors.tsv